<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Scotch">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Codebreak</title>
<!-- load bootstrap from a cdn -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Poppins:200,500" rel="stylesheet">
<!-- Styles -->
<link rel="stylesheet" href="../../css/app.css">

